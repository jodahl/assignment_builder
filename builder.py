import string
import pickle
import random

# Constants for writing HTML page outline
HTML_HEADER = """
<html><head><link rel="stylesheet" type="text/css" href="assignment_style.css"><title>
"""
HTML_HEADER_END = """
</title></head><body>
"""
HTML_FOOTER = """
</body></html>
"""


class QuestionController:
    def __init__(self):
        self.question_list = [] # Will be a list of dictionaries
        self.num_questions = 0

    def new_file(self, filename):
        self.filename = filename
        self.input_entries()
    
    # This one works when each dictionary is saved individually
    # I think I'll use this format for the program as it reduces writes
    # and allows easy appending
    def get_entries_from_file(self, filename):
        # Reads pickled data from the file
        self.filename = filename

        try:
            read_questions = 0
            #Read the file
            with open(self.filename, 'r') as f:
                while True:
                    # Read each pickled dictionary from the file, appending it to hte list
                    self.question_list.append(pickle.load(f))
                    self.num_questions = self.num_questions + 1
                    read_questions = read_questions + 1
        except IOError:
            print "{0} does not exist. Please try another file or create a new one".format(self.filename)
        except pickle.UnpicklingError:
            print "Problem reading from the file. Maybe it is corrupt?"
        except EOFError:
            print "{0} questions read.".format(read_questions)

    """
    # This one reads an entire list object from the file
    # It's what happens if you save the entire list at once
    # when it's filled with dictionaries already
    def get_entries_from_file(self, filename):
        # Reads pickled data from file
        self.filename = filename

        try:
            read_questions = 0
            #Read the file
            with open(self.filename, 'r') as f:
                self.question_list = pickle.load(f)
                self.num_questions = len(self.question_list)
        except IOError:
            print "{0} does not exist. Please try another file or create a new one".format(self.filename)
        except pickle.UnpicklingError:
            print "Problem reading from the file. Maybe it is corrupt?"
        except EOFError:
            print "{0} questions read.".format(self.num_questions)
    """
    def input_entries(self):
        while self.num_questions < 20:
            dict_container = {}
            print "Question Number {0}".format(self.num_questions + 1)
            print "----------"
            dict_container['question'] = raw_input("  Question: ")
            dict_container['right'] = raw_input("  Answer: ")
            dict_container['wrong1'] = raw_input("  Wrong Answer: ")
            dict_container['wrong2'] = raw_input("  Wrong Answer: ")
            dict_container['wrong3'] = raw_input("  Wrong Answer: ")
            dict_container['location'] = raw_input("  Location: ")
            print " "

            while True: # loops to make sure a valid option was chosen
                choice = raw_input("(N)ext Question (R)edo Question (E)xit to Menu w/o Saving (M)ain Menu > ")
                choice = string.upper(choice[0])
                # Save on next or on quit
                if choice == "N" or choice == "M":
                    self.num_questions = self.num_questions + 1
                    self.question_list.append(dict_container)
                    # Write to file
                    with open(self.filename, 'a') as f:
                        pickle.dump(dict_container, f)
                        print "Saved.\n"
                    break
                elif choice == "R":
                    break
                elif choice == "E":
                    quit = string.upper(raw_input("Exiting without saving this entry. Are you sure? (y/n) "))[0]
                    if quit == "Y":
                        break
                    else:
                        choice == "noquit"
                else:
                    print "Did not understand {0}. Please pick a valid option.".format(choice)
            if choice == "M" or choice == "E":
                break
        if self.num_questions == 20:
            print "There's 20!"


    def edit_entry(self):
        while True:
            # Print questions
            count = 1
            for question in self.question_list:
                print "{0}. {1}".format(count, question["question"])
                count = count + 1
            # Get question to edit
            print " "
            
            choice = string.upper(raw_input("(E)dit (D)elete (Q)uit and Save > "))[0]
            # Edit
            if choice == "E":
                # Question number
                q_num = int(raw_input("Question # to edit: "))
                # Make sure the chosen value is between 1 and 20
                if q_num < 1 or q_num > self.num_questions:
                    print "Please enter a valid question to edit."
                else:
                    # Print the old information and get the new for each entry
                    old_dict = self.question_list.pop(q_num-1)
                    dict_container = {}
                    print "Question Number {0}".format(q_num)
                    print "----------"
                    print "Old: {0}".format(old_dict['question'])
                    dict_container['question'] = raw_input("  Question: ")
                    print "Old: {0}".format(old_dict['right'])
                    dict_container['right'] = raw_input("  Answer: ")
                    print "Old: {0}".format(old_dict['wrong1'])
                    dict_container['wrong1'] = raw_input("  Wrong Answer: ")
                    print "Old: {0}".format(old_dict['wrong2'])
                    dict_container['wrong2'] = raw_input("  Wrong Answer: ")
                    print "Old: {0}".format(old_dict['wrong3'])
                    dict_container['wrong3'] = raw_input("  Wrong Answer: ")
                    print "Old: {0}".format(old_dict['location'])
                    dict_container['location'] = raw_input("  Location: ")
                    print " "
                    # Add it back in the same spot
                    self.question_list.insert(q_num-1,dict_container)
            # Delete
            elif choice == "D":
                q_num = int(raw_input("Question # to delete: "))
                # Make sure the question number is valid
                if q_num < 1 or q_num > self.num_questions:
                    print "Please enter a valid question to delete."
                else:
                    check_delete = string.upper(raw_input("Are you sure you want to delete #{0} (Y/N) ".format(q_num)))[0]
                    if check_delete == "Y":
                        self.question_list.pop(q_num-1)
                    else:
                        print "# {0} not deleted.".format(q_num)
            # Quit and save
            elif choice == "Q":
                with open(self.filename, 'w') as f:
                    for i in xrange(0,len(self.question_list)):
                        pickle.dump(self.question_list[i], f)
                    print "Saved.\n"
                break
            else:
                print "Did not understand your choice, please enter a proper one."


    def make_html(self):
        assignment_number = raw_input("Please enter the assignment number: ")
        name = raw_input("Please enter your name: ")
        html_file = self.filename.split('.')[0] + '.html'
        print "Making {0}.html with {1} questions.".format(html_file,
            self.num_questions)
        try:
            with open(html_file, 'w') as f:
                # Randomize questions
                self.randomize_questions()
                # Print preliminary html
                f.write(HTML_HEADER)
                # Writes the page title
                f.write("CSE 365 Assignment {0}".format(assignment_number))
                # Write the end of the header
                f.write(HTML_HEADER_END)
                prelim = """
                <div id="header">
                <span id='title'>Assignment {0}</span>
                <span id='name'>{1}</span>
                </div>
                <div id='questions'>
                """.format(assignment_number, name)
                f.write(prelim)
                question_number = 1
                # Randomize the answers in each question and write HTML
                for question in self.question_list:
                    answer_list, correct_answer = self.randomize_answers(question)
                    line1 = "[{0}] {1}".format(question_number, question['question'])
                    f.write(line1)
                    f.write("<ol type='A'>")
                    # Write possible answers
                    for answer in answer_list:
                        a_format = "<li>{0}</li>".format(answer)
                        f.write(a_format)
                    # Close out the answer list
                    f.write("</ol>")
                    # Write actual answer
                    f.write("Answer: {0}".format(correct_answer))
                    f.write("<br>")
                    # Write the location
                    f.write("Question from: {0}".format(question['location']))
                    f.write("<br><br>")
                    question_number = question_number + 1
                f.write("</div>")
                f.write(HTML_FOOTER)
                print "{0} written successfully".format(html_file)

        except IOError:
            print "Failed to write {0}. Please try again".format(html_file)



    def randomize_questions(self):
        """ Using the Fisher-Yates algorithm as described here, for the question_list in place
            http://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#The_modern_algorithm
        """
        random.seed()
        for i in xrange(len(self.question_list)-1, 1, -1):
            j = random.randint(0,i)
            self.question_list[i], self.question_list[j] = self.question_list[j], self.question_list[i]
        print "Questions randomized."

    def randomize_answers(self, question):
        """ Pass a question dictionary, use Fisher-Yates to randomize, return tuple of 
            (answer_list, answer_location)
        """
        random.seed()
        answer_list = []
        answer_location = random.randint(0,3)
        answer_list.append(question['wrong1'])
        answer_list.append(question['wrong2'])
        answer_list.append(question['wrong3'])
        # Randomize wrong answers
        for i in xrange(len(answer_list)-1, 1, -1):
            j = random.randint(0,i)
            answer_list[i], answer_list[j] = answer_list[j], answer_list[i]
        answer_list.insert(answer_location, question['right'])
        # Determine the alpha equivalent of answer_location
        if answer_location == 0:
            answer_location = "A"
        elif answer_location == 1:
            answer_location = "B"
        elif answer_location == 2:
            answer_location = "C"
        else:
            answer_location = "D"
        return (answer_list, answer_location)

    def get_filename(self):
        return self.filename

    def get_num_questions(self):
        return self.num_questions


def main():
    # Menu
    while True:
        print "(N)ew Assignment (O)pen Assignment (I)nput/(E)dit Questions (M)ake HTML (Q)uit"
        choice = string.upper(raw_input(" > "))
        if choice == "N":
            qc = QuestionController()
            filename = raw_input("Please enter the filename: ")
            qc.new_file(filename)
        elif choice == "O":
            qc = QuestionController()
            filename = raw_input("Please enter the filename: ")
            qc.get_entries_from_file(filename)
        elif choice == "I":
            try:
                qc.input_entries()
            except Exception:
                print "Please create a new file or open a file."
        elif choice == "E":
            qc.edit_entry()
        elif choice == "M":
            qc.make_html()
        elif choice == "Q":
            break
        else:
            print "Did not understand {0}. Please pick a valid option.".format(choice)

if __name__ == "__main__":
    main()