## builder.py
##### Purpose
Every week we have to create assignments in the same format for my CSE 365 class, so I wrote a python script to facilitate entry and creation. It's creation was inspired by procrastinating on homework while still seemingly being productive.

##### Features
* Export to HTML (printable as PDF in Chrome/others)
* Create up to 20 questions per assignment (this should be easily modifiable)
* Ability to edit pre-exported assignments
* Customizeable CSS

##### Usage
It should be fairly straight forward, enter the question, answer, three wrong answers, and where it came from in the text. When you select "Make" it will randomize the order of the answers for each question, randomize the order of the questions, and then output to an html file with the same filename as you chose to store the questions in.

##### Sample Output
See sample.bui for a sample question file and sample.html for a sample output

##### Modifying
Please do, let me know if you do. I could see this being used for creating multiple choice sheets quite easily. It would be easy to modify and I could do it for you if you wanted :)

##### To do
* Custom question lengths (perhaps just remove the limit?)
* GUI
